## README : Assignment1


* Ruby version 2.5.5


* Rails version 5.2.3

* This application contains two controllers that could in the future deal with different behaviors.


* Templates are written using slim-rails:
    * slim-rails gem installed through Gemfile & bundle install
   

* /index triggers the welcome controller & /goodbye route triggers the goodbye controller

* The index page display "Hello World !" and a link to the goodbye view that displayed "Goodbye World!"


* The project use git for Version Control and is available in the following repository: [Mouadh_Repo](https://bitbucket.org/mouadhkh/htw_web_assignments/)
